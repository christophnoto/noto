import {Directive} from "@angular/core";
import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors} from "@angular/forms";
import {map, Observable, of} from "rxjs";
import {User} from "../../entities/user";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AuthService} from "../auth.service";

@Directive({
  selector: '[asyncMailValidator][formControlName],[asyncMailValidator][FormControl]',
  providers: [
    {provide: NG_ASYNC_VALIDATORS, useExisting: AsyncMailValidator, multi: true}
  ]
})
export class AsyncMailValidator implements AsyncValidator {

  constructor(private store: AngularFirestore, private auth: AuthService
  ) {
  }

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    if (!control.value || control.value.trim() == "" || this.auth.getUserMail() == control.value.trim())
      return of({invalidAsync: true});
    return this.store.collection("users").doc<User>(control.value).get().pipe(map((value => value.exists ? null : {invalidAsync: true})))
  }
}
