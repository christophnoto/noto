import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {firstValueFrom, ReplaySubject} from "rxjs";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {User} from "../entities/user";
import firebase from "firebase/compat/app";
import {Project} from "../entities/project";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInObservable: ReplaySubject<boolean>;
  uuid: null | string;
  mail: null | string;


  constructor(private router: Router, private auth: AngularFireAuth, private store: AngularFirestore) {
    this.uuid = null;
    this.mail = null;

    this.loggedInObservable = new ReplaySubject<boolean>();

    this.auth.onAuthStateChanged((user) => {
      if (user) {
        this.loggedInObservable.next(true);
        this.uuid = user.uid
        this.mail = user.email;
      } else {
        this.loggedInObservable.next(false);
        this.uuid = null;
        this.mail = null;
      }

    });
  }

  login(user: any): Promise<any> {
    return this.auth.signInWithEmailAndPassword(user.mail, user.password)
      .then((result) => {
        if (result.user != null) {
          firstValueFrom(this.store.collection("users").doc<User>(result.user.email!).get()).then(value => {
            if (!value.exists) {
              console.log("not existing");
              console.log(value);
              const user: User = {
                mail: result.user!.email!,
                sharedProjects: [],
                id: result.user!.uid
              }
              this.store.collection("users").doc(result.user!.email!).set(user);
            } else {
              console.log("user already in db");
              if(value.data()!.id!=result.user!.uid){
                this.store.collection("users").doc(result.user!.email!).update({id: result.user!.uid});
              }
            }
          });
        }
        console.log('Auth Service: loginUser: success');
      })
      .catch(error => {
        console.log('Auth Service: login error...');
        console.log('error code', error.code);
        console.log('error', error);
        return {isValid: false, message: error.message};
      });
  }

  signupUser(user: any): Promise<any> {
    return this.auth.createUserWithEmailAndPassword(user.mail, user.password)
      .then((result) => {
        if (result.user != null) {
          const user: User = {mail: result.user.email!, sharedProjects: [], id: result.user.uid}
          this.store.collection("users").doc(result.user.email!).set(user);
          result.user.sendEmailVerification();
        }
      })
      .catch(error => {
        console.log('Auth Service: signup error', error);
        return {isValid: false, message: error.message};
      });
  }

  async updatePassword(user: any): Promise<string | void> {
    try {
      let currentUser: firebase.User | null = await this.auth.currentUser;
      const credential = firebase.auth.EmailAuthProvider.credential(this.getUserMail(), user.oldPassword);
      let authenticatedUser = await currentUser!.reauthenticateWithCredential(credential);
      await authenticatedUser.user?.updatePassword(user.newPassword);
    } catch (e) {
      console.error('error', e);
      return (e as Error).message
    }
  }

  async deleteAccount(user: any): Promise<string | void> {
    try {
      let currentUser: firebase.User | null = await this.auth.currentUser;
      const credential = firebase.auth.EmailAuthProvider.credential(this.getUserMail(), user.oldPassword);
      let authenticatedUser = await currentUser!.reauthenticateWithCredential(credential);
      await this.store.collection("users").doc(this.getUserMail()).delete();
      let projects = await firstValueFrom(this.store.collection<Project>("projects", ref => ref.where('owner', '==', this.getUserId())).get());
      for (let project of projects.docs) {
        await this.store.collection("projects").doc(project.id).delete();
      }
      await authenticatedUser.user?.delete();
    } catch (e) {
      console.error('error', e);
      return (e as Error).message
    }
  }

  async resetPassword(resetMail: string): Promise<string | void> {
    try {
      await this.auth.sendPasswordResetEmail(resetMail)
    } catch (e) {
      console.error('error', e);
      return (e as Error).message
    }
  }

  logout(): void {
    this.auth.signOut().then(r => {
      this.router.navigate(['/login'])
    });
  }

  getUserId(): string {
    return <string>this.uuid;
  }

  getUserMail(): string {
    return <string>this.mail;
  }

  async getCurrentAuthStatus(): Promise<boolean> {
    return await this.auth.authState != null;
  }

}
