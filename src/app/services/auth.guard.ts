import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {
  }

  async canActivate(): Promise<boolean> {
    console.log("guard gets called");
    if (await this.authService.getCurrentAuthStatus()) {
      return true;
    } else {
      console.log("unallowed access to page, back to login");
      this.router.navigate(['/login'])
      return false;
    }
  }

}
