import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss', './../app.component.scss']
})
export class UserSettingsComponent {

  processing: boolean = false;

  settingsForm = new FormGroup({
    'oldPassword': new FormControl('', Validators.required),
    'newPassword': new FormControl('', Validators.required),
  });
  firebaseErrorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
    this.firebaseErrorMessage = '';
  }

  async onSubmit() {
    if (this.processing)
      return;

    this.processing = true;

    if (this.settingsForm == null || this.settingsForm!.invalid){
      this.processing = false;
      return;
    }


    let result = await this.authService.updatePassword(this.settingsForm.value);
    if (!result) {
      alert("Updated successfully.");
      this.router.navigate(['/project-page']);
    } else {
      this.firebaseErrorMessage = result!;
    }

    this.processing = false;
  }

  async deleteAccount() {
    if (this.processing)
      return;

    this.processing = true;

    if(!this.settingsForm.controls['oldPassword'].valid){
      this.processing = false;
      return;
    }


    if (confirm("Are you sure to delete your account?")) {
      let result = await this.authService.deleteAccount(this.settingsForm.value);
      if (!result) {
        alert("Account has been deleted.");
        this.router.navigate(['/login']);
      } else {
        this.firebaseErrorMessage = result!;
      }
    }

    this.processing = false;
  }

}
