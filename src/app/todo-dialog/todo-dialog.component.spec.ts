import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TodoDialogComponent, TodoDialogData} from './todo-dialog.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {Importance, Todo} from "../entities/todo";

describe('TodoDialogComponent', () => {
  let component: TodoDialogComponent;
  let fixture: ComponentFixture<TodoDialogComponent>;

  beforeEach(async () => {
    const todo: Todo = {
      dueUntil: null, importance: Importance.High,
      title: "TITLE",
      checked: false, text: "TODO"
    }
    const data: TodoDialogData = {
      enableDelete: false,
      todo: todo
    }
    await TestBed.configureTestingModule({
      declarations: [ TodoDialogComponent ],
      imports: [
        MatDialogModule
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {todo: data}
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
