import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Importance, Todo} from "../entities/todo";
import firebase from "firebase/compat/app";
import Timestamp = firebase.firestore.Timestamp;

@Component({
  selector: 'app-todo-dialog',
  templateUrl: './todo-dialog.component.html',
  styleUrls: ['./todo-dialog.component.scss', './../app.component.scss']
})
export class TodoDialogComponent {

  private backupTodo: Partial<Todo> = { ...this.data.todo };

  selectedDate?: Date;

  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TodoDialogData
  ) {
    if(data.todo.dueUntil){
      this.selectedDate = data.todo.dueUntil.toDate();
    }
  }

  cancel(): void {
    console.log(this.data.todo.dueUntil);
    this.data.todo.text=this.backupTodo.text;
    this.data.todo.title=this.backupTodo.title;
    this.data.todo.importance=this.backupTodo.importance;
    this.dialogRef.close(null);
  }

  submit(): void {
    if(this.selectedDate){
      this.data.todo.dueUntil=Timestamp.fromDate(this.selectedDate);
    } else {
      this.data.todo.dueUntil=null;
    }
    console.log(this.selectedDate);
    this.dialogRef.close({ todo: this.data.todo });
  }

  public get Importance() {
    return Importance;
  }

  newDate(value: any): void {
  console.log(value);
  }
}

export interface TodoDialogData {
  todo: Partial<Todo>;
  enableDelete: boolean;
}

export interface TodoDialogResult {
  todo: Todo;
  delete?: boolean;
}
