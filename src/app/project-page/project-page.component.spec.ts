import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProjectPageComponent} from './project-page.component';
import {MatDialogModule} from "@angular/material/dialog";
import {AngularFireModule} from "@angular/fire/compat";
import {environment} from "../../environments/environment";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "../services/auth.service";

describe('ProjectpageComponent', () => {
  let component: ProjectPageComponent;
  let fixture: ComponentFixture<ProjectPageComponent>;
  let mockAuthService;

  beforeEach(async () => {

    mockAuthService = jasmine.createSpyObj(['getUserMail', 'getUserId']);
    mockAuthService.getUserMail.and.returnValue('testValue');
    mockAuthService.getUserId.and.returnValue('testId');

    await TestBed.configureTestingModule({
      declarations: [ProjectPageComponent],
      imports: [
        MatDialogModule,
        AngularFireModule.initializeApp(environment.firebase),
        RouterTestingModule,
      ],
      providers: [{
        provide: AuthService,
        useValue: mockAuthService
      }],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
