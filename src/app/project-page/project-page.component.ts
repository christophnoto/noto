import {Component} from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AuthService} from "../services/auth.service";
import {combineLatest, fromEvent, map, Observable, startWith} from "rxjs";
import {Project} from "../entities/project";
import {MatDialog} from "@angular/material/dialog";
import {ProjectDialogComponent, ProjectDialogResult} from "../project-dialog/project-dialog.component";
import {arrayRemove, arrayUnion} from "@angular/fire/firestore";
import {User} from "../entities/user";

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss', './../app.component.scss']
})
export class ProjectPageComponent {

  columns = window.innerWidth >= 599 ? 2 : 1;
  rowHeight = window.innerWidth >= 599 ? "1:1" : "2:1";
  showDeleted: boolean = false;

  projects = this.store.collection("projects", ref => ref.where('owner', '==', this.authService.getUserId())).valueChanges({idField: 'id'}) as Observable<Project[]>;

  sharedProjects?: Observable<Project[]>;

  constructor(private dialog: MatDialog, private store: AngularFirestore, private authService: AuthService) {
    fromEvent(window, 'resize')
      .pipe(
        map(event => (event.target as any).innerWidth),
        startWith(window.innerWidth)
      ).subscribe(width => {
      this.columns = width >= 599 ? 2 : 1;
      this.rowHeight = width >= 599 ? "1:1" : "2:1";
    });


    this.store.collection("users").doc<User>(this.authService.getUserMail()).valueChanges().subscribe(
      next => {
        if (next) {
          let observables: Observable<Project>[] = [];
          next.sharedProjects.forEach(projectId => {
            observables.push(this.store.collection("projects").doc<Project>(projectId).valueChanges({idField: 'id'}) as Observable<Project>);
          })
          this.sharedProjects = combineLatest(observables);
        }
      }
    );
  }

  newProject(): void {
    if (!this.authService.uuid)
      return;
    const dialogRef = this.dialog.open(ProjectDialogComponent, {
      width: '300px',
      data: {
        project: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: ProjectDialogResult | undefined) => {
        if (!result) {
          return;
        }

        result.project.owner = this.authService.getUserId();
        result.project.removed = false;
        this.store.collection("projects").add(result.project).then(value => {
          console.log(result);
          if (result.mails) {
            for (let mail of result.mails) {
              this.store.collection("users").doc(mail).update({sharedProjects: arrayUnion(value.id)})
            }
          }
        });
      });
  }

  editProject(project: Project): void {
    if (!this.authService.uuid)
      return;

    const dialogRef = this.dialog.open(ProjectDialogComponent, {
      width: '300px',
      data: {
        project,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: ProjectDialogResult | undefined) => {
      if (!result) {
        return;
      }
      if (result.delete != undefined) {
        project.removed = result.delete;
        this.store.collection("projects").doc(project.id).update(project);
      } else {
        this.store.collection("projects").doc(project.id).update(result.project);
        if (result.mails) {
          for (let mail of result.mails) {
            this.store.collection("users").doc(mail).update({sharedProjects: arrayUnion(project.id)})
          }
          if (result.initialMails) {
            for (let removedUserMail of result.initialMails.filter(x => !result.mails!.includes(x))) {
              this.store.collection("users").doc(removedUserMail).update({sharedProjects: arrayRemove(project.id)})
            }
          }
        }
      }
    });
  }


}
