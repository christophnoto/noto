import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Project} from "../entities/project";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {firstValueFrom} from "rxjs";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {User} from "../entities/user";

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.scss', './../app.component.scss']
})
export class ProjectDialogComponent implements OnInit {

  projectForm: FormGroup;

  disabled: boolean = false;
  initialMails: string[] = [];

  private backupProject: Partial<Project> = {...this.data.project};

  constructor(
    public dialogRef: MatDialogRef<ProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProjectDialogData, private formBuilder: FormBuilder, private store: AngularFirestore
  ) {
    this.projectForm = this.formBuilder.group({
      name: this.data.project.name,
      description: this.data.project.description,
      collaborators: this.formBuilder.array([])
    });
  }

  ngOnInit(): void {
    console.log(this.data.project);
    if(this.data.project.sharedWith){
      for (let uid of this.data.project.sharedWith){
        firstValueFrom(this.store.collection<User>("users", ref => ref.where('id', '==', uid)).get()).then(value => {
          for (let user of value.docs) {
            this.initialMails.push(user.data().mail);
            this.addCollaborator(user.data().mail);
          }
        });
      }
    }
    }

  cancel(): void {
    this.data.project.name = this.backupProject.name;
    this.data.project.description = this.backupProject.description;
    this.data.project.sharedWith = this.backupProject.sharedWith;
    this.dialogRef.close(null);
  }

  get collaborators(): FormArray {
    return this.projectForm.controls["collaborators"] as FormArray;
  }

  createCollaborator(mail?:string): FormGroup {
    return this.formBuilder.group({
      mail: [mail ? mail : '', Validators.required]
    });
  }

  addCollaborator(mail?: string) {
    this.collaborators.push(this.createCollaborator(mail))
  }

  deleteCollaborator(collaboratorIndex: number) {
    this.collaborators.removeAt(collaboratorIndex);
  }

  async onSubmit() {
    if (this.disabled || !this.projectForm.valid) {
      return;
    }
    this.disabled=true;
    this.data.project.name=this.projectForm.controls['name'].value;
    this.data.project.description=this.projectForm.controls['description'].value;
    this.data.project.sharedWith=[];
    let mails = [];
    for (let value of this.projectForm.controls['collaborators'].value){
      let user = await firstValueFrom(this.store.collection("users").doc<User>(value.mail).get())
      if(!this.data.project.sharedWith.includes(<string>user.data()?.id)){
        mails.push(value.mail);
        this.data.project.sharedWith.push(<string>user.data()?.id);
      }
    }
    this.dialogRef.close({project: this.data.project, mails: mails, initialMails: this.initialMails});
    this.disabled = false;
  }
}

export interface ProjectDialogData {
  project: Partial<Project>;
  enableDelete: boolean;
}

export interface ProjectDialogResult {
  project: Project;
  mails?: string[];
  initialMails?: string[];
  delete?: boolean;
}
