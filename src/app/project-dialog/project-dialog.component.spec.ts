import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProjectDialogComponent, ProjectDialogData} from './project-dialog.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {Project} from "../entities/project";
import {ReactiveFormsModule} from "@angular/forms";
import {AngularFireModule} from "@angular/fire/compat";
import {environment} from "../../environments/environment";

describe('ProjectDialogComponent', () => {
  let component: ProjectDialogComponent;
  let fixture: ComponentFixture<ProjectDialogComponent>;

  beforeEach(async () => {
    const project: Project = {
      description: "Test", name: "Test", owner: "Test", removed: false, sharedWith: []
    }
    const data: ProjectDialogData = {
      enableDelete: false,
      project: project
    }
    await TestBed.configureTestingModule({
      declarations: [ProjectDialogComponent],
      imports: [
        MatDialogModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {project: data}
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
