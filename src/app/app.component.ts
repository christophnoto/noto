import {Component} from '@angular/core';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'noto';
  isLoggedIn : boolean | undefined;

  constructor(private authService: AuthService) {
    authService.loggedInObservable.subscribe(next => {
      this.isLoggedIn = next
    })
  }

  logout(): void {
    this.authService.logout();
  }
}
