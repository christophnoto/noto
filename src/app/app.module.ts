import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {TodoComponent} from './todo/todo.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatCardModule} from "@angular/material/card";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {TodoDialogComponent} from './todo-dialog/todo-dialog.component';
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {environment} from '../environments/environment';
import {AngularFireModule} from "@angular/fire/compat";
import {AngularFirestoreModule} from "@angular/fire/compat/firestore";
import {SignupComponent} from './signup/signup.component';
import {LoginComponent} from './login/login.component';
import {TodoPageComponent} from './todo-page/todo-page.component';
import {ProjectPageComponent} from './project-page/project-page.component';
import {MatGridListModule} from "@angular/material/grid-list";
import {ProjectDialogComponent} from './project-dialog/project-dialog.component';
import {ProjectComponent} from './project/project.component';
import {AsyncMailValidator} from "./services/validators/async-mail.validator";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {MatMenuModule} from "@angular/material/menu";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MAT_DATE_LOCALE, MatNativeDateModule} from "@angular/material/core";

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodoDialogComponent,
    SignupComponent,
    LoginComponent,
    TodoPageComponent,
    ProjectPageComponent,
    ProjectDialogComponent,
    ProjectComponent,
    AsyncMailValidator,
    UserSettingsComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule,
    DragDropModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    MatGridListModule,
    MatButtonToggleModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'de-AT'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
