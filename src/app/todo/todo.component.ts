import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Importance, Todo} from "../entities/todo";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {

  @Input() todo: Todo | null = null;
  @Input() projectId: string | null | undefined = null;
  @Output() edit = new EventEmitter<Todo>();

  constructor(private store: AngularFirestore, private authService: AuthService) { }

  checkedChanged(event: MatCheckboxChange): void{
    if(this.todo!=null){
      this.todo!.checked=event.checked
      this.store.collection("projects").doc(this.projectId!).collection("todos").doc(this.todo!.id).update(this.todo!);
    }
  }

  getIconForTodo(): string | void{
    if(this.todo?.importance==Importance.High){
      return "priority_high";
    } else if (this.todo?.importance==Importance.Low){
      return "low_priority"
    }
  }

}
