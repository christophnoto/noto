import firebase from "firebase/compat/app";
import Timestamp = firebase.firestore.Timestamp;

export interface Todo {
  id?: string;
  checked: boolean;
  title: string;
  text: string;
  importance: Importance;
  dueUntil: Timestamp | null;
}

export enum Importance {
  High,
  Middle,
  Low,
}
