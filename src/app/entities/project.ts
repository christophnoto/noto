export interface Project {
  id?: string;
  removed: boolean;
  owner: string;
  name: string;
  description: string;
  sharedWith: string[];
}
