import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Project} from "../entities/project";
import {Router} from "@angular/router";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent {

  mouseOverButton: boolean = false;

  @Input() project: Project | null = null;
  @Input() editable: boolean = true;
  @Output() edit = new EventEmitter<Project>();

  constructor(private router: Router) {
  }

  forwardToProjectTodos() {
    if (!this.mouseOverButton) {
      if (this.project != null) {
        this.router.navigate(['/todo-page', {id: this.project.id}]);
      }
    }
  }
}
