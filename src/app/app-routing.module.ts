import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./services/auth.guard";
import {LoginComponent} from "./login/login.component";
import {SignupComponent} from "./signup/signup.component";
import {TodoPageComponent} from "./todo-page/todo-page.component";
import {ProjectPageComponent} from "./project-page/project-page.component";
import {UserSettingsComponent} from "./user-settings/user-settings.component";

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'settings', component: UserSettingsComponent, canActivate: [AuthGuard]},
  {path: 'todo-page', component: TodoPageComponent, canActivate: [AuthGuard]},
  {path: 'project-page', component: ProjectPageComponent, canActivate: [AuthGuard]},
  {path: '**', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
