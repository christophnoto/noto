import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', './../app.component.scss']
})
export class LoginComponent {

  processing: boolean = false;

  loginForm = new FormGroup({
    'mail': new FormControl('', [Validators.required, Validators.email]),
    'password': new FormControl('', Validators.required),
  });
  firebaseErrorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
    this.firebaseErrorMessage = '';
  }

  login() {
    if (this.loginForm == null || this.loginForm!.invalid)
      return;

    this.authService.login(this.loginForm.value).then((result) => {
      console.log("Not forwaring")
      console.log(result)
      if (result == undefined){
        console.log("Not forwaring2");
        this.router.navigate(['/project-page']);
      }
      else if (result.isValid == false)
        this.firebaseErrorMessage = result.message;
    }).catch(e => {
      console.log(e);
    });
  }

  async forgotPassword() {
    if (this.processing)
      return;

    this.processing = true;

    if (this.loginForm == null || this.loginForm!.controls['mail'].invalid){
      this.processing = false;
      return;
    }


    let result = await this.authService.resetPassword(this.loginForm!.controls['mail'].value);
    if(!result) {
      alert("A mail to reset your password has been sent. Please also check your spam folder.")
    } else {
      this.firebaseErrorMessage = result;
    }

    this.processing = false;
  }
}
