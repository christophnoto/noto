import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {TodoDialogComponent, TodoDialogResult} from "../todo-dialog/todo-dialog.component";
import {Todo} from "../entities/todo";
import {firstValueFrom, Observable} from "rxjs";
import {AuthService} from "../services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Project} from "../entities/project";

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.scss', './../app.component.scss']
})
export class TodoPageComponent implements OnInit {

  project: Observable<Project> | undefined;
  todos: Observable<Todo[]> | undefined;
  projectId: string | null | undefined;

  constructor(private dialog: MatDialog, private store: AngularFirestore, private authService: AuthService, private route: ActivatedRoute, private router: Router) {
    this.projectId = this.route.snapshot.paramMap.get('id');
  }

  async ngOnInit() {
    if (this.projectId) {
      const doc = await firstValueFrom(this.store.collection("projects").doc(this.projectId).get());
      if(!doc.exists) {
        await this.router.navigate(['/project-page']);
      }
      this.project = this.store.collection("projects").doc(this.projectId).valueChanges({idField: 'id'}) as Observable<Project>;
      this.todos = this.store.collection("projects").doc(this.projectId).collection("todos").valueChanges({idField: 'id'}) as Observable<Todo[]>;
    } else {
      await this.router.navigate(['/project-page']);
    }


  }

  newTodo(): void {
    if (!this.authService.uuid || !this.projectId)
      return;
    const dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '300px',
      data: {
        todo: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: TodoDialogResult | undefined) => {
        if (!result) {
          return;
        }
        result.todo.checked = false;
        if(this.projectId){
          this.store.collection("projects").doc(this.projectId).collection("todos").add(result.todo);
        }
      });
  }

  editTask(todo: Todo): void {
    if (!this.authService.uuid)
      return;

    const dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '300px',
      data: {
        todo,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TodoDialogResult | undefined) => {
      if (!result || !this.projectId) {
        return;
      }
      if (result.delete) {
        this.store.collection("projects").doc(this.projectId).collection("todos").doc(todo.id).delete();
      } else {
        console.log(result.todo)
        this.store.collection("projects").doc(this.projectId).collection("todos").doc(todo.id).update(todo);
      }
    });
  }


}
