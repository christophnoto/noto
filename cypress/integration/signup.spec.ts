import firebase from "firebase/compat";
import {environment} from "../../src/environments/environment";

describe('Login e2e tests', () => {

  let testMail : string = Math.random().toString(36).slice(2)+"safe@boofx.com"
  let testPassword : string = "Hallo1234"
  firebase.initializeApp(environment.firebase);

  beforeEach(() => {
    cy.visit('/signup')
  })


  it('Create account and log in', () => {
    cy.get('[data-testid="mailInput"]').type(testMail);
    cy.get('[data-testid="passwordInput"]').type(testPassword);
    cy.get('[data-testid="submitForm"]').click();
    cy.wait(3000);
    cy.url().should('include', 'project-page');
  })
})
